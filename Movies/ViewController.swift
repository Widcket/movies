//
//  ViewController.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    let coreData = CDDataSource(cdManager: CDManager(container: CDStack.container))
    
    var repository: MoviesRepository?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        repository = MoviesRepository(api: ApiDataSource<MoviesApi>(plugins: [TimeoutPlugin()]),
                              coreData: coreData)
        
        makeTestRequest()
    }

    func makeTestRequest() {
        repository?.getMovies(refresh: true).subscribe { movies in
            // print(movies)
        }
        .disposed(by: disposeBag)
        
        coreData.loadMovie(id: 299537).subscribe { movie in
            // print(movie)
        }
        .disposed(by: disposeBag)
    }
    
}

