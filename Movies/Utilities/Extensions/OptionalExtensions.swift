//
//  OptionalExtensions.swift
//  Movies
//
//  Created by Rita Zerrizuela on 19/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

fileprivate enum OptionalError: Error {
    
    case nilValue
    
}

extension Optional {
    
    func unwrap() throws -> Wrapped {
        if let value = self {
            return value
        }
        else {
            throw OptionalError.nilValue
        }
    }
    
    func unwrap(_ defaultValue: @autoclosure () -> Wrapped) -> Wrapped {
        return self ?? defaultValue()
    }
    
}
