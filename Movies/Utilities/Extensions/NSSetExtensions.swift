//
//  NSSetExtensions.swift
//  Movies
//
//  Created by Rita Zerrizuela on 19/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

extension Optional where Wrapped == NSSet {
    
    func array<T>(of: T.Type) -> [T] {
        return (self?.allObjects as? [T]) ?? [T]()
    }
    
}
