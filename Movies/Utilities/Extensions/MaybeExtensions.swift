//
//  MaybeExtensions.swift
//  Movies
//
//  Created by Rita Zerrizuela on 20/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import RxSwift

extension PrimitiveSequenceType where TraitType == MaybeTrait {
    
    func ifEmptyOrError(switchTo alternative: PrimitiveSequence<MaybeTrait, ElementType>)
        -> PrimitiveSequence<MaybeTrait, ElementType> {
        return self.ifEmpty(switchTo: alternative).catchError({ error in alternative })
    }
    
    func ifEmptyOrError(switchTo alternative: PrimitiveSequence<SingleTrait, ElementType>)
        -> PrimitiveSequence<SingleTrait, ElementType> {
        return self.ifEmpty(switchTo: alternative).catchError({ error in alternative })
    }
    
    func logError() -> PrimitiveSequence<TraitType, ElementType> {
        return self.do(onError: { error in debugPrint(error) })
    }
    
}
