//
//  AppConfig.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import Keys

final class AppConfig {
    
    private init() {}
    
    static var TMDBApiKey: String { return MoviesKeys().tMDBApiKey }
    
    static let TMDBApiUrl = "https://api.themoviedb.org/3"
    static let TMDBImageUrl = "https://image.tmdb.org/t/p"
    static let networkTimeout = TimeInterval(5)
    
}
