//
//  CDManager.swift
//  Movies
//
//  Created by Rita Zerrizuela on 19/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

final class CDManager {
    
    let container: NSPersistentContainer
    
    lazy var context: NSManagedObjectContext = {
        let backgroundContext = container.newBackgroundContext()
        
        backgroundContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        return backgroundContext
    }()
    
    init(container: NSPersistentContainer) {
        self.container = container
    }
    
    func load<M: CDConvertible>(for entity: M.E.Type,
                                as model: M.Type,
                                with predicate: NSPredicate? = nil) -> Maybe<[M]> {
        return Maybe<[M]>.create { [unowned context] maybe in
            context.perform {
                let request: NSFetchRequest<NSFetchRequestResult> = entity.fetchRequest()
                
                request.predicate = predicate
                
                let asyncRequest = NSAsynchronousFetchRequest<NSFetchRequestResult>(fetchRequest: request) { result in
                    guard result.operationError == nil else {
                        maybe(.error(result.operationError!))
                        
                        return
                    }
                    
                    guard let entities = result.finalResult as? [M.E] else {
                        maybe(.completed)
                        
                        return
                    }
                    
                    maybe(.success(entities.compactMap { try? model.init(from: $0) }))
                }
                
                do {
                    try context.execute(asyncRequest)
                }
                catch let error {
                    maybe(.error(error))
                }
            }
            
            return Disposables.create()
        }
    }
    
    func load<M: CDConvertible>(for entity: M.E.Type,
                                as model: M.Type,
                                with predicate: NSPredicate? = nil) -> Maybe<M> {
        return Maybe<M>.create { [unowned context] maybe in
            context.perform {
                let request: NSFetchRequest<NSFetchRequestResult> = entity.fetchRequest()
                
                request.predicate = predicate
                
                let asyncRequest = NSAsynchronousFetchRequest<NSFetchRequestResult>(fetchRequest: request) { result in
                    guard result.operationError == nil else {
                        maybe(.error(result.operationError!))
                        
                        return
                    }
                    
                    guard let result = result.finalResult as? [M.E], let entity = result.first else {
                        maybe(.completed)
                        
                        return
                    }
                    
                    do {
                        let model = try model.init(from: entity)
                        
                        maybe(.success(model))
                    }
                    catch let error {
                        maybe(.error(error))
                    }
                }
                
                do {
                    try context.execute(asyncRequest)
                }
                catch let error {
                    maybe(.error(error))
                }
            }
            
            return Disposables.create()
        }
    }
    
    func upsert<E: ModelLoadable>(_ models: [E.M],
                                  for entity: E.Type) -> Completable {
        return Completable.create { [unowned context] completable in
            context.perform {
                for model in models {
                    let entity = E.init(context: context)
                    
                    entity.loadData(from: model, with: context)
                }
                
                do {
                    try context.save()
                    
                    completable(.completed)
                }
                catch let error {
                    completable(.error(error))
                }
            }
            
            return Disposables.create()
        }
    }
    
    func delete<E: NSManagedObject>(for entity: E.Type,
                                    with predicate: NSPredicate? = nil) -> Completable {
        return Completable.create { [unowned context] completable in
            context.perform {
                let request: NSFetchRequest<NSFetchRequestResult> = entity.fetchRequest()
                
                request.predicate = predicate
                
                do {
                    try context.execute(NSBatchDeleteRequest(fetchRequest: request))
                    
                    completable(.completed)
                }
                catch let error {
                    completable(.error(error))
                }
            }
            
            return Disposables.create()
        }
    }
    
}
