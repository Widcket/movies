//
//  CDSpokenLanguage.swift
//  Movies
//
//  Created by Rita Zerrizuela on 19/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import CoreData

extension CDSpokenLanguage: ModelLoadable {
    
    func loadData(from model: SpokenLanguage, with context: NSManagedObjectContext) {
        self.code = model.code
        self.name = model.name
    }
    
}

extension SpokenLanguage: CDConvertible {

    init(from cdEntity: CDSpokenLanguage) throws {
        self.code = try cdEntity.code.unwrap()
        self.name = try cdEntity.name.unwrap()
    }

}
