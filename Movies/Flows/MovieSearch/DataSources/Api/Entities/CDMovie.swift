//
//  CDMovie.swift
//  Movies
//
//  Created by Rita Zerrizuela on 19/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import CoreData

extension CDMovie: ModelLoadable {
    
    func loadData(from model: Movie, with context: NSManagedObjectContext) {
        self.id = Int64(model.id)
        self.backdropSmallUrl = model.backdropSmallUrl
        self.backdropMediumUrl = model.backdropMediumUrl
        self.backdropLargeUrl = model.backdropLargeUrl
        self.budget = model.budget as NSNumber?
        
        self.addToGenres(NSSet(array: model.genres.map {
            let entity = CDGenre(context: context)
            
            entity.loadData(from: $0, with: context)
            
            return entity
        }))

        self.originalLanguage = model.originalLanguage
        self.originalTitle = model.originalTitle
        self.overview = model.overview
        self.popularity = model.popularity
        self.posterSmallUrl = model.posterSmallUrl
        self.posterMediumUrl = model.posterMediumUrl
        self.posterLargeUrl = model.posterLargeUrl
        
        self.addToProductionCompanies(NSSet(array: model.productionCompanies.unwrap([]).map {
             let entity = CDProductionCompany(context: context)
            
            entity.loadData(from: $0, with: context)
            
            return entity
        }))
         
        self.addToProductionCountries(NSSet(array: model.productionCountries.unwrap([]).map {
            let entity = CDProductionCountry(context: context)
            
            entity.loadData(from: $0, with: context)
            
            return entity
        }))
        
        self.releaseDate = model.releaseDate
        self.revenue = model.revenue as NSNumber?
        self.runtime = model.runtime as NSNumber?
        
        self.addToSpokenLanguages(NSSet(array: model.spokenLanguages.unwrap([]).map {
            let entity = CDSpokenLanguage(context: context)
            
            entity.loadData(from: $0, with: context)
            
            return entity
        }))

        self.status = model.status
        self.tagline = model.tagline
        self.title = model.title
        self.voteAverage = model.voteAverage
        self.voteCount = Int64(model.voteCount)
    }
    
}

extension Movie: CDConvertible {
    
    init(from cdEntity: CDMovie) throws {
        self.id = Int(cdEntity.id)
        self.backdropSmallUrl = cdEntity.backdropSmallUrl
        self.backdropMediumUrl = cdEntity.backdropMediumUrl
        self.backdropLargeUrl = cdEntity.backdropLargeUrl
        self.budget = cdEntity.budget?.intValue
        
        self.genres = cdEntity.genres
            .array(of: CDGenre.self)
            .compactMap { try? Genre(from: $0) }
        
        self.originalLanguage = try cdEntity.originalLanguage.unwrap()
        self.originalTitle = try cdEntity.originalTitle.unwrap()
        self.overview = try cdEntity.overview.unwrap()
        self.popularity = cdEntity.popularity
        self.posterSmallUrl = cdEntity.posterSmallUrl
        self.posterMediumUrl = cdEntity.posterMediumUrl
        self.posterLargeUrl = cdEntity.posterLargeUrl
        
        self.productionCompanies = cdEntity.productionCompanies
            .array(of: CDProductionCompany.self)
            .compactMap { try? ProductionCompany(from: $0) }
        
        self.productionCountries = cdEntity.productionCountries
            .array(of: CDProductionCountry.self)
            .compactMap { try? ProductionCountry(from: $0) }
        
        self.releaseDate = cdEntity.releaseDate
        self.revenue = cdEntity.revenue?.intValue
        self.runtime = cdEntity.runtime?.intValue
        
        self.spokenLanguages = cdEntity.spokenLanguages
            .array(of: CDSpokenLanguage.self)
            .compactMap { try? SpokenLanguage(from: $0) }
        
        self.status = cdEntity.status
        self.tagline = cdEntity.tagline
        self.title = try cdEntity.title.unwrap()
        self.voteAverage = cdEntity.voteAverage
        self.voteCount = Int(cdEntity.voteCount)
    }
    
}
