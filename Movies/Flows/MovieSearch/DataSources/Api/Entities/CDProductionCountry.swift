//
//  CDProductionCountry.swift
//  Movies
//
//  Created by Rita Zerrizuela on 19/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import CoreData

extension CDProductionCountry: ModelLoadable {
    
    func loadData(from model: ProductionCountry, with context: NSManagedObjectContext) {
        self.code = model.code
        self.name = model.name
    }
    
}

extension ProductionCountry: CDConvertible {

    init(from cdEntity: CDProductionCountry) throws {
        self.code = try cdEntity.code.unwrap()
        self.name = try cdEntity.name.unwrap()
    }

}
