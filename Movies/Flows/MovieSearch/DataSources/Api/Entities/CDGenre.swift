//
//  CDGenre.swift
//  Movies
//
//  Created by Rita Zerrizuela on 19/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import CoreData

extension CDGenre: ModelLoadable {
    
    func loadData(from model: Genre, with context: NSManagedObjectContext) {
        self.id = Int64(model.id)
        self.name = model.name
    }
    
}

extension Genre: CDConvertible {

    init(from cdEntity: CDGenre) throws {
        self.id = Int(cdEntity.id)
        self.name = cdEntity.name
    }

}
