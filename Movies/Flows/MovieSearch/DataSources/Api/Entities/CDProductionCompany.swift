//
//  CDProductionCompany.swift
//  Movies
//
//  Created by Rita Zerrizuela on 19/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import CoreData

extension CDProductionCompany: ModelLoadable {
    
    func loadData(from model: ProductionCompany, with context: NSManagedObjectContext) {
        self.id = Int64(model.id)
        self.logoSmallUrl = model.logoSmallUrl
        self.logoMediumUrl = model.logoMediumUrl
        self.logoLargeUrl = model.logoLargeUrl
        self.name = entity.name
        self.originCountry = model.originCountry
    }
    
}

extension ProductionCompany: CDConvertible {

    init(from cdEntity: CDProductionCompany) throws {
        self.id = Int(cdEntity.id)
        self.logoSmallUrl = cdEntity.logoSmallUrl
        self.logoMediumUrl = cdEntity.logoMediumUrl
        self.logoLargeUrl = cdEntity.logoLargeUrl
        self.name = try cdEntity.name.unwrap()
        self.originCountry = try cdEntity.originCountry.unwrap()
    }

}
