//
//  CDConvertible.swift
//  Movies
//
//  Created by Rita Zerrizuela on 19/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import CoreData

protocol CDConvertible {

    associatedtype E: NSManagedObject

    init(from cdEntity: E) throws

}
