//
//  CDDataSource.swift
//  Movies
//
//  Created by Rita Zerrizuela on 20/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import RxSwift

final class CDDataSource {
    
    private let cdManager: CDManager
    
    init(cdManager: CDManager) {
        self.cdManager = cdManager
    }
    
    func loadAllMovies() -> Maybe<[Movie]>  {
        return runInBackground(cdManager.load(for: CDMovie.self, as: Movie.self))
    }
    
    func loadMovie(id: Int64) -> Maybe<Movie> {
        let predicate = NSPredicate(format: "id == %d", id)
        
        return runInBackground(cdManager.load(for: CDMovie.self, as: Movie.self, with: predicate))
    }
    
    func upsertAllMovies(_ movies: [Movie]) -> Completable {
        return runInBackground(cdManager.upsert(movies, for: CDMovie.self))
    }
    
    func upsertMovie(_ movie: Movie) -> Completable {
        return runInBackground(upsertAllMovies([movie]))
    }
    
    func deleteAllMovies() -> Completable {
        return runInBackground(cdManager.delete(for: CDMovie.self))
    }
    
    func deleteMovie(id: Int64) -> Completable {
        let predicate = NSPredicate(format: "id == %@", id)
        
        return runInBackground(cdManager.delete(for: CDMovie.self, with: predicate))
    }
    
    func deleteAllGenres() -> Completable {
        return runInBackground(cdManager.delete(for: CDGenre.self))
    }
    
    func deleteAllProductionCompanies() -> Completable {
        return runInBackground(cdManager.delete(for: CDProductionCompany.self))
    }
    
    func deleteAllProductionCountries() -> Completable {
        return runInBackground(cdManager.delete(for: CDProductionCountry.self))
    }
    
    func deleteAllSpokenLanguages() -> Completable {
        return runInBackground(cdManager.delete(for: CDSpokenLanguage.self))
    }
    
    func clearDatabase() -> Completable {
        return deleteAllSpokenLanguages()
            .andThen(deleteAllProductionCountries())
            .andThen(deleteAllProductionCompanies())
            .andThen(deleteAllGenres())
            .andThen(deleteAllMovies())
    }
    
    private func runInBackground<T>(_ maybe: Maybe<T>) -> Maybe<T> {
        return maybe.subscribeOn(SerialDispatchQueueScheduler.init(qos: .userInitiated))
    }
    
    private func runInBackground(_ completable: Completable) -> Completable {
        return completable.subscribeOn(SerialDispatchQueueScheduler.init(qos: .userInitiated))
    }
    
}
