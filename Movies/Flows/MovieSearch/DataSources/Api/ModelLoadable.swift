//
//  ModelLoadable.swift
//  Movies
//
//  Created by Rita Zerrizuela on 19/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import CoreData

protocol ModelLoadable: NSManagedObject {
    
    associatedtype M: CDConvertible
    
    static var entityName: String { get }
    
    func loadData(from model: M, with context: NSManagedObjectContext)
    
}

extension ModelLoadable {
    
    static var entityName: String { return String(describing: self) }
    
}
