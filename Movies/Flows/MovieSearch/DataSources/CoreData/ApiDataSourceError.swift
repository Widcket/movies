//
//  ApiDataSourceError.swift
//  Movies
//
//  Created by Rita Zerrizuela on 20/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

enum ApiDataSourceError: Error {
    
    case jsonMappingFailed(response: ApiResponse)
    
}
