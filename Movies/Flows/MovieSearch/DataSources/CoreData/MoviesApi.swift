//
//  MoviesApi.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt
import Moya

enum MoviesApi {
    
    case nowPlaying(page: Int)
    case details(id: Int)
    
}

extension MoviesApi: TargetType {
    
    var baseURL: URL { return URL(string: AppConfig.TMDBApiUrl)! }
    var method: Moya.Method { return .get }
    var headers: [String : String]? { return ["Accept": "application/json"] }
    var parameterEncoding: ParameterEncoding { return URLEncoding.default }
    var sampleData: Data { return Data() }
    
    var path: String {
        let movie = "/movie"
        
        switch self {
        case .nowPlaying(_): return "\(movie)/now_playing"
        case .details(let id): return "\(movie)/\(id)"
        }
    }
    
    var task: Task {
        var parameters: [String: Any] = ["api_key": AppConfig.TMDBApiKey]
        
        switch self {
        case .nowPlaying(let page):
            parameters["page"] = page
            
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        case .details(_):
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        }
    }
    
}

extension MoviesApi: ApiTargetType {
    
    var timeout: TimeInterval { return AppConfig.networkTimeout }
    var dispatchQueue: DispatchQueue { return DispatchQueue.global(qos: .userInitiated) }
    var retries: RepeatBehavior { return .exponentialDelayed(maxCount: 3, initial: 0.5, multiplier: 1.0) }
    var retryScheduler: SchedulerType { return ConcurrentDispatchQueueScheduler(qos: .userInitiated) }
    
    /*
    static func registerDependecies(in container: Container) {
        
    }
    */
    
}

extension MoviesApi {
    
    enum ImageSizes: String {
        
        case small = "w92"
        case medium = "w154"
        case large = "original"
        
    }
    
    var keyPath: String? {
        switch self {
        case .nowPlaying: return "results"
        case .details: return nil
        }
    }
    
}
