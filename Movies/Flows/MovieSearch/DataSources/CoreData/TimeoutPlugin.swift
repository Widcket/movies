//
//  TimeoutPlugin.swift
//  Movies
//
//  Created by Rita Zerrizuela on 20/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import Moya

final class TimeoutPlugin: PluginType {
    
    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        var mutableRequest = request
        
        mutableRequest.timeoutInterval = AppConfig.networkTimeout
        
        return mutableRequest
    }
    
}
