//
//  ApiDataSource.swift
//  Movies
//
//  Created by Rita Zerrizuela on 20/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt
import Moya

typealias ApiResponse = Moya.Response

final class ApiDataSource<Api: ApiTargetType>: MoyaProvider<Api> {
    
    func request(_ endpoint: Api) -> Observable<ApiResponse> {
        return rx.request(endpoint, callbackQueue: endpoint.dispatchQueue)
            .asObservable()
            .retry(endpoint.retries, scheduler: endpoint.retryScheduler)
            .catchError({ error -> Observable<Response> in
                guard let moyaError = error as? MoyaError else { return Observable.error(error) }
                
                switch moyaError {
                case .jsonMapping(let response):
                    return Observable.error(ApiDataSourceError.jsonMappingFailed(response: response))
                case .objectMapping(_, let response):
                    return Observable.error(ApiDataSourceError.jsonMappingFailed(response: response))
                default:
                    return Observable.error(error)
                }
            })
    }
    
}
