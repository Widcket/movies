//
//  APIGenre.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

extension Genre: Decodable {
    
    enum CodingKeys: String, CodingKey {
        
        case id, name
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int.self, forKey: .id)
        name = try? container.decode(String.self, forKey: .name)
    }
    
    func encode(to encoder: Encoder) throws {}
    
}
