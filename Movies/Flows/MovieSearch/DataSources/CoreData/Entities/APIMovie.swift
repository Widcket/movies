//
//  APIMovie.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

extension Movie: Decodable {
    
    enum CodingKeys: String, CodingKey {
        
        case id, budget, genres
        case backdropPath = "backdrop_path"
        case genreIds = "genre_ids"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview, popularity
        case posterPath = "poster_path"
        case productionCompanies = "production_companies"
        case productionCountries = "production_countries"
        case releaseDate = "release_date"
        case revenue, runtime
        case spokenLanguages = "spoken_languages"
        case status, tagline, title
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int.self, forKey: .id)
        
        backdropSmallUrl = URL(string: AppConfig.TMDBImageUrl)?
            .appendingPathComponent(MoviesApi.ImageSizes.small.rawValue)
            .appendingPathComponent(try container.decode(String.self, forKey: .backdropPath))
        
        backdropMediumUrl = URL(string: AppConfig.TMDBImageUrl)?
            .appendingPathComponent(MoviesApi.ImageSizes.medium.rawValue)
            .appendingPathComponent(try container.decode(String.self, forKey: .backdropPath))
        
        backdropLargeUrl = URL(string: AppConfig.TMDBImageUrl)?
            .appendingPathComponent(MoviesApi.ImageSizes.large.rawValue)
            .appendingPathComponent(try container.decode(String.self, forKey: .backdropPath))
        
        budget = try? container.decode(Int.self, forKey: .budget)
        
        genres = try (try? container.decode([Genre].self, forKey: .genres)) ??
            container.decode([Int].self, forKey: .genreIds).map({ Genre(id: $0, name: nil) })
        
        originalLanguage = try container.decode(String.self, forKey: .originalLanguage)
        originalTitle = try container.decode(String.self, forKey: .originalTitle)
        overview = try container.decode(String.self, forKey: .overview)
        popularity = try container.decode(Double.self, forKey: .popularity)
        
        posterSmallUrl = URL(string: AppConfig.TMDBImageUrl)?
            .appendingPathComponent(MoviesApi.ImageSizes.small.rawValue)
            .appendingPathComponent(try container.decode(String.self, forKey: .posterPath))
        
        posterMediumUrl = URL(string: AppConfig.TMDBImageUrl)?
            .appendingPathComponent(MoviesApi.ImageSizes.medium.rawValue)
            .appendingPathComponent(try container.decode(String.self, forKey: .posterPath))
        
        posterLargeUrl = URL(string: AppConfig.TMDBImageUrl)?
            .appendingPathComponent(MoviesApi.ImageSizes.large.rawValue)
            .appendingPathComponent(try container.decode(String.self, forKey: .posterPath))
        
        productionCompanies = try? container.decode([ProductionCompany].self, forKey: .productionCompanies)
        productionCountries = try? container.decode([ProductionCountry].self, forKey: .productionCountries)
        releaseDate = try? DateFormatter.yyyyMMdd.date(from: container.decode(String.self, forKey: .releaseDate))
        revenue = try? container.decode(Int.self, forKey: .revenue)
        runtime = try? container.decode(Int.self, forKey: .runtime)
        spokenLanguages = try? container.decode([SpokenLanguage].self, forKey: .spokenLanguages)
        status = try? container.decode(String.self, forKey: .status)
        tagline = try? container.decode(String.self, forKey: .tagline)
        title = try container.decode(String.self, forKey: .title)
        voteAverage = try container.decode(Double.self, forKey: .voteAverage)
        voteCount = try container.decode(Int.self, forKey: .voteCount)
    }
    
    func encode(to encoder: Encoder) throws {}
    
}
