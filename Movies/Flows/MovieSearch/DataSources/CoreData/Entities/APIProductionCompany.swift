//
//  APIProductionCompany.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

extension ProductionCompany: Decodable {
    
    enum CodingKeys: String, CodingKey {
        
        case id, name
        case logoPath = "logo_path"
        case originCountry = "origin_country"
        
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int.self, forKey: .id)
        
        logoSmallUrl = URL(string: AppConfig.TMDBImageUrl)?
            .appendingPathComponent(MoviesApi.ImageSizes.small.rawValue)
            .appendingPathComponent(try container.decode(String.self, forKey: .logoPath))
        
        logoMediumUrl = URL(string: AppConfig.TMDBImageUrl)?
            .appendingPathComponent(MoviesApi.ImageSizes.medium.rawValue)
            .appendingPathComponent(try container.decode(String.self, forKey: .logoPath))
        
        logoLargeUrl = URL(string: AppConfig.TMDBImageUrl)?
            .appendingPathComponent(MoviesApi.ImageSizes.large.rawValue)
            .appendingPathComponent(try container.decode(String.self, forKey: .logoPath))
        
        name = try container.decode(String.self, forKey: .name)
        originCountry = try container.decode(String.self, forKey: .originCountry)
    }
    
    func encode(to encoder: Encoder) throws {}
    
}
