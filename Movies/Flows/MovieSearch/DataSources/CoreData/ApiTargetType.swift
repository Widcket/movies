//
//  ApiTargetType.swift
//  Movies
//
//  Created by Rita Zerrizuela on 20/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt
import Moya

protocol ApiTargetType: TargetType {
    
    var timeout: TimeInterval { get }
    var dispatchQueue: DispatchQueue { get }
    var retries: RepeatBehavior { get }
    var retryScheduler: SchedulerType { get }
    
}
