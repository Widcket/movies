//
//  MoviesRepository.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import Moya
import CoreData
import RxSwift
import RxSwiftExt

// TODO: Throw a special error if offline

final class MoviesRepository {
    
    private let api: ApiDataSource<MoviesApi>
    private let coreData: CDDataSource
    
    private let jsonDecoder = JSONDecoder()
    
    init(api: ApiDataSource<MoviesApi>, coreData: CDDataSource) {
        self.api = api
        self.coreData = coreData
    }
    
    func getMovies(page: Int = 1, refresh: Bool = false) -> Single<[Movie]> {
        let moviesEndpoint = MoviesApi.nowPlaying(page: page)
        let cdDataSource = coreData.loadAllMovies()
        
        let apiDataSource = api.request(moviesEndpoint)
            .map([Movie].self, atKeyPath: moviesEndpoint.keyPath, using: jsonDecoder, failsOnEmptyData: true)
            .flatMap { [unowned coreData] movies in
                return coreData.clearDatabase()
                    .andThen(coreData.upsertAllMovies(movies)
                    .andThen(Observable.just(movies)))
            }
            .asSingle()
        
        return refresh ? apiDataSource : cdDataSource.ifEmptyOrError(switchTo: apiDataSource)
    }
    
    func getMovie(id: Int, refresh: Bool = false) -> Maybe<Movie> {
        let movieEndpoint = MoviesApi.details(id: id)
        let cdDataSource = coreData.loadMovie(id: Int64(id))
        
        let apiDataSource = api.request(movieEndpoint)
            .map(Movie.self, atKeyPath: movieEndpoint.keyPath, using: jsonDecoder, failsOnEmptyData: true)
            .flatMap { [unowned coreData] movie in
                return coreData.upsertMovie(movie).andThen(Observable.just(movie))
            }
            .asMaybe()
        
        return refresh ? apiDataSource : cdDataSource.ifEmptyOrError(switchTo: apiDataSource)
    }
    
}
