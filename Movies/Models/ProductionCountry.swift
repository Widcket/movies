//
//  ProductionCountry.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct ProductionCountry {
    
    let code: String
    let name: String
    
}

extension ProductionCountry: Equatable {
    
    static func ==(lhs: ProductionCountry, rhs: ProductionCountry) -> Bool {
        return lhs.code == rhs.code
    }
    
}
