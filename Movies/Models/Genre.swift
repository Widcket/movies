//
//  Genre.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct Genre {
    
    let id: Int
    let name: String?
    
}

extension Genre: Equatable {
    
    static func ==(lhs: Genre, rhs: Genre) -> Bool {
        return lhs.id == rhs.id
    }
    
}
