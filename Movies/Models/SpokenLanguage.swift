//
//  SpokenLanguage.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct SpokenLanguage {
    
    let code: String
    let name: String
    
}

extension SpokenLanguage: Equatable {
    
    static func ==(lhs: SpokenLanguage, rhs: SpokenLanguage) -> Bool {
        return lhs.code == rhs.code
    }
    
}
