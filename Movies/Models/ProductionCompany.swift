//
//  ProductionCompany.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct ProductionCompany {
    
    let id: Int
    let logoSmallUrl: URL?
    let logoMediumUrl: URL?
    let logoLargeUrl: URL?
    let name: String
    let originCountry: String
    
}

extension ProductionCompany: Equatable {
    
    static func ==(lhs: ProductionCompany, rhs: ProductionCompany) -> Bool {
        return lhs.id == rhs.id
    }
    
}
