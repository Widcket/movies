//
//  Movie.swift
//  Movies
//
//  Created by Rita Zerrizuela on 18/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct Movie {
    
    let id: Int
    let backdropSmallUrl: URL?
    let backdropMediumUrl: URL?
    let backdropLargeUrl: URL?
    let budget: Int?
    let genres: [Genre]
    let originalLanguage: String
    let originalTitle: String
    let overview: String
    let popularity: Double
    let posterSmallUrl: URL?
    let posterMediumUrl: URL?
    let posterLargeUrl: URL?
    let productionCompanies: [ProductionCompany]?
    let productionCountries: [ProductionCountry]?
    let releaseDate: Date?
    let revenue: Int?
    let runtime: Int?
    let spokenLanguages: [SpokenLanguage]?
    let status: String?
    let tagline: String?
    let title: String
    let voteAverage: Double
    let voteCount: Int
    
}

extension Movie: Equatable {
    
    static func ==(lhs: Movie, rhs: Movie) -> Bool {
        return lhs.id == rhs.id
    }
    
}
